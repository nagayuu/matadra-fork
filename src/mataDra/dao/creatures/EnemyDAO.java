package mataDra.dao.creatures;

import java.util.List;

import mataDra.dao.DAO;
import mataDra.entity.creatures.CreatureEntity.Attribute;
import mataDra.entity.creatures.CreatureEntity.BattleStateType;
import mataDra.entity.creatures.EnemyEntity;

public class EnemyDAO extends DAO<EnemyEntity> {

	@Override
	public List<T> registerAll() {
		// TODO 自動生成されたメソッド・スタブ
		setEntity(new EnemyEntity(0, "マタンゴ(炎)", 100, 3, 10, 100, 1, null, BattleStateType.MOVABLE, Attribute.FIRE, "A", 0));
		setEntity(new EnemyEntity(1, "ポイズンマタンゴ(炎)", 100, 3, 10, 100, 1, null, BattleStateType.MOVABLE, Attribute.FIRE, "A", 1));
		setEntity(new EnemyEntity(2, "マージマタンゴ(炎)", 100, 3, 10, 100, 1, null, BattleStateType.MOVABLE, Attribute.FIRE, "A", 2));
		setEntity(new EnemyEntity(3, "キングマタンゴ(炎)", 1000, 3, 10, 100, 1, null, BattleStateType.MOVABLE, Attribute.FIRE, "A", 3));
		setEntity(new EnemyEntity(4, "りゅうおう(炎)", 1000, 3, 10, 100, 1, null, BattleStateType.MOVABLE, Attribute.FIRE, "A", 4));
		setEntity(new EnemyEntity(0, "マタンゴ(水)", 100, 3, 10, 100, 1, null, BattleStateType.MOVABLE, Attribute.WATER, "A", 0));
		setEntity(new EnemyEntity(1, "ポイズンマタンゴ(水)", 100, 3, 10, 100, 1, null, BattleStateType.MOVABLE, Attribute.WATER, "A", 1));
		setEntity(new EnemyEntity(2, "マージマタンゴ(水)", 100, 3, 10, 100, 1, null, BattleStateType.MOVABLE, Attribute.WATER, "A", 2));
		setEntity(new EnemyEntity(3, "キングマタンゴ(水)", 1000, 3, 10, 100, 1, null, BattleStateType.MOVABLE, Attribute.WATER, "A", 3));
		setEntity(new EnemyEntity(4, "りゅうおう(水)", 1000, 3, 10, 100, 1, null, BattleStateType.MOVABLE, Attribute.WATER, "A", 4));
		setEntity(new EnemyEntity(0, "マタンゴ(木)", 100, 3, 10, 100, 1, null, BattleStateType.MOVABLE, Attribute.WOOD, "A", 0));
		setEntity(new EnemyEntity(1, "ポイズンマタンゴ(木)", 100, 3, 10, 100, 1, null, BattleStateType.MOVABLE, Attribute.WOOD, "A", 1));
		setEntity(new EnemyEntity(2, "マージマタンゴ(木)", 100, 3, 10, 100, 1, null, BattleStateType.MOVABLE, Attribute.WOOD, "A", 2));
		setEntity(new EnemyEntity(3, "キングマタンゴ(木)", 1000, 3, 10, 100, 1, null, BattleStateType.MOVABLE, Attribute.WOOD, "A", 3));
		setEntity(new EnemyEntity(4, "りゅうおう(木)", 1000, 3, 10, 100, 1, null, BattleStateType.MOVABLE, Attribute.WOOD, "A", 4));

	}
	
	public void findByAttribute(int index,Attribute attribute){
		
	}


}
