package mataDra.logics.sounds;

import mataDra.dao.sounds.SoundDAO;
import mataDra.logics.DAOLogic;

public class SoundLogic extends DAOLogic<SoundDAO> {

	//コンストラクタ
	public SoundLogic(){
		//登録したデータを読み込む
		setDao(new SoundDAO());
		//データの数を記録する
		setCount(getDao().registerAll().size());
	}


	//表示するオブジェクトを返す
	public String convert(int index) {
		// TODO 自動生成されたメソッド・スタブ

		return getDao().findByIndex(index).getSoundEffect();
	}



}
